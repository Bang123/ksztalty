require('dotenv').config();

var express = require("express");
var app = express();
const path = require('path');

app.use(express.static("dist"));

app.listen(process.env.PORT, process.env.IP, function(){
    console.log("App has started!!!");
});
