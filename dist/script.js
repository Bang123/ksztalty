var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

var diagnosticPara = document.querySelector('.output');

let kwadrat = document.querySelector("#kwadrat").parentElement;
let okrag = document.querySelector("#okrag").parentElement;
let trojkat = document.querySelector("#trojkat").parentElement;
let trapez = document.querySelector("#trapez").parentElement;

var testBtn = document.querySelector('button');

function testSpeech() {
  testBtn.disabled = true;

  for(let i = 0; i < document.getElementsByClassName("ksztalt").length; i++) {
      document.getElementsByClassName("ksztalt")[i].className = "ksztalt";
  }

  mruganiePrzycisku(testBtn);

  diagnosticPara.textContent = '...przetwarzam';

  var recognition = new SpeechRecognition();
  recognition.lang = 'pl-PL';
  recognition.interimResults = false;
  recognition.maxAlternatives = 1;

  recognition.start();

  recognition.onresult = function(event) {
    var speechResult = event.results[0][0].transcript;
    diagnosticPara.textContent = 'Usłyszałem: ' + speechResult + '.';
    switch (speechResult) {
        case "kwadrat":
            kwadrat.className += " podswietlone";
            break;
        case "okrąg":
            okrag.className += " podswietlone";
            break;
        case "trójkąt":
            trojkat.className += " podswietlone";
            break;
        case "trapez":
            trapez.className += " podswietlone";
            break;
        default:
            diagnosticPara.textContent += " Nie znalazłem takiego kształtu.";
    }


    console.log('Confidence: ' + event.results[0][0].confidence);
  }

  recognition.onspeechend = function() {
    console.log("Test skonczony");
    recognition.stop();
    testBtn.disabled = false;
    testBtn.className = "wait";
    testBtn.innerHTML = '<i class="fas fa-microphone fa-7x"></i>';
  }

  recognition.onerror = function(event) {
    testBtn.disabled = false;
    testBtn.className = "wait";
    testBtn.innerHTML = '<i class="fas fa-microphone fa-7x"></i>';
    diagnosticPara.textContent = 'Wystąpił błąd w trakcie przetwarzania: ' + event.error;
  }

  recognition.onaudiostart = function(event) {
      //Fired when the user agent has started to capture audio.
      console.log('SpeechRecognition.onaudiostart');
  }

  recognition.onaudioend = function(event) {
      //Fired when the user agent has finished capturing audio.
      console.log('SpeechRecognition.onaudioend');
  }

  recognition.onend = function(event) {
      //Fired when the speech recognition service has disconnected.
      console.log('SpeechRecognition.onend');
  }

  recognition.onnomatch = function(event) {
      //Fired when the speech recognition service returns a final result with no significant recognition. This may involve some degree of recognition, which doesn't meet or exceed the confidence threshold.
      console.log('SpeechRecognition.onnomatch');
  }

  recognition.onsoundstart = function(event) {
      //Fired when any sound — recognisable speech or not — has been detected.
      console.log('SpeechRecognition.onsoundstart');
  }

  recognition.onsoundend = function(event) {
      //Fired when any sound — recognisable speech or not — has stopped being detected.
      console.log('SpeechRecognition.onsoundend');
  }

  recognition.onspeechstart = function (event) {
      //Fired when sound that is recognised by the speech recognition service as speech has been detected.
      console.log('SpeechRecognition.onspeechstart');
  }
  recognition.onstart = function(event) {
      //Fired when the speech recognition service has begun listening to incoming audio with intent to recognize grammars associated with the current SpeechRecognition.
      console.log('SpeechRecognition.onstart');
  }
}

mruganiePrzycisku = function(przycisk) {
    // console.log(przycisk);
    let tmp = {przycisk: przycisk};
    if(przycisk.disabled != false) {
        for(var i = 1; i < 10; i++) {
            setTimeout(function() {
                // console.log("Dzień dobry");
                // console.log(this);
                // console.log(this.przycisk.disabled);
                if(this.przycisk.disabled != false) {
                    // console.log(this.przycisk.className);
                    if(this.przycisk.className == "wait") {
                        this.przycisk.className = "test"
                    } else {
                        // console.log("ustawiam na wait");
                        this.przycisk.className = "wait"
                    }
                }
            }.bind(tmp), 300*i)
        }
    }
}

testBtn.addEventListener('click', testSpeech);
